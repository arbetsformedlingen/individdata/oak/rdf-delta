# RDF Delta / Fuseki for local developement

## Run
Start the rdf-delta patch server and the fuseki SPARQL server by
`docker compose up`

The rdf-delta patch server will create a directory called store in the current directory where the database files will be stored.

The fuseki configuration file in the fuseki directory is configured to store the patch logs in the store/SOLID directory.

## Background
Prject Oak aim to implement a data infrastructure used by the Swedish authoroties and private actors for individual-centered information based on SOLID.

The project will initially use the Community Solid Server (CSS) for the SOLID pod storage.

CSS uses RDF data internally for storing information and it is suggested to use RDF data stores in production environments. These RDF storages oftes has a standardised SPARQL frontend for accessing (and storing) data.

When the deployment goes live it has to have a HA configuration and hence needs a HA storage backend.

[RDF Delta](https://afs.github.io/rdf-delta/) offers HA configurations of the [Apache Jena Fuseki](https://jena.apache.org/documentation/fuseki2/index.html) SPARQL server.

The initial developemnt configuration has only one delta server backing a single fuseki server.
